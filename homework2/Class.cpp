#include <iostream>
#include <sstream>
using namespace std;

/**
	* @brief	              : This class contains some information of students
*/
class Student
{
private:
	int age;
	int standard;
	string first_name, last_name;
public:
	/**
		* @brief	          : The age variable of this object is assigned a value with this set function.
		* @param   a    	  : integer variable
	*/
	void set_age(int a) 
	{
		age = a;
	}
	/**
		* @brief	          : The first_name variable of this object is assigned a value with this set function.
		* @param   FirstName  : String variable
	*/
	void set_first_name(string FirstName)
	{
		first_name = FirstName;
	}
	/**
		* @brief	          : The last_name variable of this object is assigned a value with this set function.
		* @param   LastName   : String variable
	*/
	void set_last_name(string LastName)
	{
		last_name = LastName;
	}
	/**
		* @brief	          : The standard variable of this object is assigned a value with this set function.
		* @param    Std       : integer variable
	*/
	void set_standard(int Std)
	{
		standard = Std;
	}
	/**
		* @brief	          : Returns age variable
	*/
	int get_age() { return age; }
	/**
		* @brief	          : Returns standard variable
	*/
	int get_standard() { return standard; }
	/**
		* @brief	          : Returns first_name variable
	*/
	string get_first_name() { return first_name; }
	/**
		* @brief	          : Returns last_name variable
	*/
	string get_last_name() { return last_name; }

	/**
		* @brief	          : This function returns the string consisting of the above elements, separated by a comma(,).
	*/

	string to_string()
	{
		stringstream Str;
		Str << age << "," << first_name << "," << last_name << "," << standard;
		return Str.str();
	}

};

int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	return 0;
}
