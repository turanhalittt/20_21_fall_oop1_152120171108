#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

using namespace std;
/**
	* @brief	              : This function finds the biggest number of four numbers
	* @param     a,b,c,d 	  : integer variable
	* @returns                : Returns the variable biggestNum
*/
int  max_of_four(int a, int b, int c, int d)
{

	int biggestNum;
	if (a > b && a > c && a > d)
		biggestNum = a;
	else if (b > a && b > c && b > d)
		biggestNum = b;
	else if (c > a && c > b && c > d)
		biggestNum = c;
	else
		biggestNum = d;

	return biggestNum;
}

int main()
{

	int a, b, c, d;

	cin >> a >> b >> c >> d;

	int result = max_of_four(a, b, c, d);
	cout << result << endl;
	system("pause");
}