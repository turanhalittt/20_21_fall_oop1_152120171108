#include <stdio.h>
#include<stdlib.h>
#include<iostream>

using namespace std;
/**
	* @brief	              : This function finds the biggest number of four numbers
	* @param     *a     	  : integer pointer variable
	* @param     *b     	  : integer pointer variable
*/
void update(int *a, int *b) {

	int total;
	total = *a + *b;

	if (*a > *b)
	{
		*b = *a - *b;

	}
	else
	{
		*b = *b - *a;
	}
	*a = total;
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	cin>> a>> b;
	update(pa, pb);
	printf("%d\n%d", a, b);

	return 0;
}