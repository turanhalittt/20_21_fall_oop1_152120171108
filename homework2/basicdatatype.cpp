#include <iostream>
#include <cstdio>
#include<bits/stdc++.h>
using namespace std;
using namespace std;

int main() {

	int num1;
	long long int num2;
	char ch;
	float num3;
	double num4;

	cin >> num1 >> num2 >> ch >> num3 >> num4;

	cout << num1 << endl << num2 << endl << ch << endl;
	cout << fixed << setprecision(3) << num3 << endl;
	cout << fixed << setprecision(9) << num4;

	return 0;
}
