/**
*@mainpage OOB LAB HOMEWORK 2
*
*1. These codes contain solutions to some of the questions in hackerrank.
*2. There are some informations of author of the code on the "related pages".
*
*/
/**
* @page     : Introduction
* @author   : Halitcan Turan ----> Number : 152120171108 ----> E-mail : turanhalittt@gmail.com
* @date     : 11 November 2020 Wednesday
* @version  : v1.0
*/


#include <iostream>
#include <cstdio>
using namespace std;

int main()
{

	cout << "Hello, World!";

	return 0;
}
