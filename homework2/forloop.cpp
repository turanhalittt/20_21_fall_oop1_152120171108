#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

using namespace std;

int main()
{

	int firstnum, secondnum;
	string numbers[9] = { "one","two","three","four","five","six","seven","eight","nine" };

	cin >> firstnum >> secondnum;

	if (firstnum < secondnum && firstnum>=1 &&secondnum>=1 )
	{
		for (int i = firstnum; i <= secondnum; i++)
		{
			if (i<10)
			{
				cout << numbers[i-1]<<endl;
			}
			else
			{
				if (i % 2 == 0)
					cout << "even\n";
				
				else 
					cout << "odd\n";
			}
		}
	}
	else
		cout << "Second number must bigger than the first number" << endl;
	

	system("pause");
}