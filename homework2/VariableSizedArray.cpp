#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
	int n, q, b;
	cin >> n >> q;

	int** arr = new int*[n];
	for (int i = 0; i < n; i++)
	{
		int a;
		cin >> a;
		int* arr2 = new int[a];
		for (int j = 0; j < a; j++)
		{
			cin >> b;
			arr2[j] = b;
		}
		*(arr + i) = arr2;
	}

	for (int i = 0; i < q; i++)
	{
		int r, s;
		cin >> r >> s;
		cout << arr[r][s] << endl;
	}
	return 0;
}