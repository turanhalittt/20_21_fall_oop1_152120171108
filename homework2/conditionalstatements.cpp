#include <bits/stdc++.h>

using namespace std;



int main()
{
	int num;
	cin >> num;

	string arr[10] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine","Greater than 9" };

	if (num <= 9 && num >= 1) {
		cout << arr[num - 1];
	}
	else {
		cout << arr[9];
	}

	return 0;
}