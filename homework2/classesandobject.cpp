#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;
/**
	* @brief	              : This class contains some information of students
*/
class Student {
private:
	int scores[5];
	int sum;
public:
	/**
		* @brief	          : This function calculates the sum of the values in scores array
		* @returns            : Returns the variable sum

	*/
	int calculateTotalScore()
	{
		for (int i = 0; i < 5; i++)
		{
			sum = sum + scores[i];
		}
		return sum;
	}
	/**
		* @brief	          : This function reads and stores the values in the scores array
	*/
	void input()
	{
		for (int i = 0; i < 5; i++) {
			cin >> scores[i];
		}
	}
};
int main() {
	int n; // number of students
	cin >> n;
	Student *s = new Student[n]; // an array of n students

	for (int i = 0; i < n; i++) {
		s[i].input();
	}

	// calculate kristen's score
	int kristen_score = s[0].calculateTotalScore();

	// determine how many students scored higher than kristen
	int count = 0;
	for (int i = 1; i < n; i++) {
		int total = s[i].calculateTotalScore();
		if (total > kristen_score) {
			count++;
		}
	}

	// print result
	cout << count;

	return 0;
}
