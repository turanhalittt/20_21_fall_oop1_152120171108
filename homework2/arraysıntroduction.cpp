#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

using namespace std;

int main()
{

	int N;

	cin >> N;
	cout << endl;

	int *ptr = new int[N];

	for (int i = 0; i < N; i++)
	{
		cin >> ptr[i];
	}

	for (int i = N-1; i >= 0; i--)
	{
		cout << ptr[i] << " ";
	}

	system("pause");
}