#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

/**
	* @brief	              : This vector finds the biggest number of four numbers
	* @param    str      	  : String variable
	* @returns                : Returns the variable max
*/

vector<int> parseInts(string str)
{
	stringstream string(str);
	vector<int> number;
	char character;
	int temp;
	while (string >> temp)
	{
		number.push_back(temp);
		string >> character;
	}
	return number;
}

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << "\n";
	}

	return 0;
}